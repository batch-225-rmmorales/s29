db.users.insertMany([
  {
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
      phone: "87654321",
      email: "janedoe@gmail.com",
    },
    courses: ["CSS", "Javascript", "Python"],
    department: "HR",
  },
  {
    firstName: "Stephen",
    lastName: "Hawking",
    age: 50,
    contact: {
      phone: "87654321",
      email: "stephenhawking@gmail.com",
    },
    courses: ["Python", "React", "PHP"],
    department: "HR",
  },
  {
    firstName: "Neil",
    lastName: "Armstrong",
    age: 82,
    contact: {
      phone: "87654321",
      email: "neilarmstrong@gmail.com",
    },
    courses: ["React", "Laravel", "Sass"],
    department: "HR",
  },
]);

// query users with s in first name or d in last name
db.users.find(
  {
    $or: [
      { firstName: { $regex: "s", $options: "$i" } },
      { lastName: { $regex: "d", $options: "$i" } },
    ],
  },
  { firstName: 1, lastName: 1, _id: 0 }
);

// users from HR dept and their age is greater than or equal to 70
// dont need $and to perform this
db.users.find({ department: "HR", age: { $gte: 70 } });

// Find users with the letter e in their first name and has an age of less than or equal to 30.
// dont need $and
db.users.find({
  firstName: { $regex: "e", $options: "$i" },
  age: { $lte: 30 },
});
