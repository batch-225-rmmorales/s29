// code along
// query operators and field projection

// Inserting data through our database
db.users.insertMany([
			{
				firstName: "Jane",
				lastName: "Doe",
				age: 21,
				contact: {
				    phone: "87654321",
				    email: "janedoe@gmail.com"
				},
				courses: [ "CSS", "Javascript", "Python" ],
				department: "none"
			},
		    {
		        firstName: "Stephen",
		        lastName: "Hawking",
		        age: 50,
		        contact: {
		            phone: "87654321",
		            email: "stephenhawking@gmail.com"
		        },
		        courses: [ "Python", "React", "PHP" ],
		        department: "none"
		    },
		    {
		        firstName: "Neil",
		        lastName: "Armstrong",
		        age: 82,
		        contact: {
		            phone: "87654321",
		            email: "neilarmstrong@gmail.com"
		        },
		        courses: [ "React", "Laravel", "Sass" ],
		        department: "none"
		    }
		]);

// section: comparison query operator

// $gt/$gte/$lt/$lte
db.users.find({age: {$gt: 50}})
db.users.find({age: {$gte: 50}})

// $ne not equal


// $in
db.users.find({age: {$in:[82,50]}})


// logical query operators
// $or operator
db.users.find({$or:[{firstName:"Neil"}, {lastName:"Hawking"}]})

db.users.find( {$or: [ { firstName: "Neil" }, { age: 50 } ] });
- Allows us to find documents that match a single criteria from multiple provided search criteria.
	- Syntax
		db.collectionName.find({ $or: [ { fieldA: valueB }, { fieldB: valueB } ] });

$and
match multiple criteria in a single field
db.users.find({$and:[{age: {$ne:82}},{age: {$gt:30}}]})

//field projection
// include/add specific fields only

db.users.find({criteria},{field:1})

db.users.find(
	{firstName:"Jane"},
	{firstName:1}
)
//exclusion 0 or false naman
//_id:0

db.users.find(
	{firstName:"Jane"},
	{"contact.phone":1,_id:0}
)

// evaluation query operators
// $regex operator
// match a pattern

db.users.find({firstName:{$regex:'N'}})

//case insensitive

db.users.find({firstName:{$regex:'j',$options:'$i'}})

// [SECTION] Evaluation Query Operators

// $regex operator
/*
	- Allows us to find documents that match a specific string patter using regular expressions.
	- $options: '$i' - is for Case insensitivity to match upper and lower cases.

	- $options: '$x' - "Extended" capability to ignore all white space characters in the $regex pattern unless escaped or included in a character class
*/

// Case sensitive query
db.users.find({ firstName: { $regex: 'N'}} );


// Case insensitive query
db.users.find